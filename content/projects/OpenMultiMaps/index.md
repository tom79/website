+++
name = "OpenMultiMaps"
description = "Display maps with OpenStreetMap."
icon = "OpenMultiMaps.svg"
source = "framagit.org/tom79/openmaps"
translations = "crowdin.com/project/openmaps"
issues = "framagit.org/tom79/openmaps/-/issues"
[downloads]
fdroid = "f-droid.org/en/packages/app.fedilab.openmaps/"
+++
