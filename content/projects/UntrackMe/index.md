+++
name = "UntrackMe"
description = "Transform links to alternative frontends."
icon = "UntrackMe.svg"
source = "framagit.org/tom79/nitterizeme"
translations = "crowdin.com/project/nitterizeme"
issues = "framagit.org/tom79/nitterizeme/-/issues"
[downloads]
fdroid = "f-droid.org/en/packages/app.fedilab.nitterizeme/"
+++
