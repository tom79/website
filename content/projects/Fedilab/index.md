+++
name = "Fedilab"
description = "A multi-accounts client for Mastodon, Pleroma, Friendica and Pixelfed."
icon = "Fedilab.svg"
source = "codeberg.org/tom79/fedilab"
translations = "hosted.weblate.org/engage/fedilab/"
issues = "codeberg.org/tom79/Fedilab/issues"
[downloads]
fdroid = "f-droid.org/en/packages/fr.gouv.etalab.mastodon/"
gplay = "play.google.com/store/apps/details?id=app.fedilab.android"
+++
