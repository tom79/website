+++
name = "TubeLab"
description = " App for all Peertube instances."
icon = "TubeLab.svg"
source = "framagit.org/tom79/fedilab-tube"
translations = "crowdin.com/project/tubelab"
issues = "framagit.org/tom79/fedilab-tube/-/issues"
[downloads]
fdroid = "f-droid.org/en/packages/app.fedilab.tubelab/"
gplay = "play.google.com/store/apps/details?id=app.fedilab.tubelab"
+++
