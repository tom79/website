+++
name = "FediPlan"
description = "Schedule Mastodon and Pleroma posts."
icon = "FediPlan.svg"
source = "framagit.org/tom79/fediplan"
domains = ["plan.fedilab.app"]
translations = "crowdin.com/project/fediplan"
issues = "framagit.org/tom79/fediplan/-/issues"
+++
