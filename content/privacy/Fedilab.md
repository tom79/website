+++
aliases = ["/page/privacy"]
+++
##### Stored data
Only basic information from connected accounts are stored locally on the device. These data remain strictly confidential and are only usable by the application. Removing the application leads to the deletion of these data.

No logins and passwords are stored locally. They are only used to get the access token. This token can be revoked at any time with Mastodon.

##### Connections
All connections are proceed through SSL encryptions to an instance. The application will not connect to an instance that presents a non valid certificate.

##### Android Permissions
- `ACCESS_NETWORK_STATE` :<br>
        _Used to know if the device is connected to the WIFI._

- `INTERNET` :
        Used to communicate with the API.

- `WRITE_EXTERNAL_STORAGE` :
        Used to store media and to move the application on the SD card

- `READ_EXTERNAL_STORAGE` :
        Used to pick up media for uploading them.

- `BOOT_COMPLETED` :
        Used to start the notification service when the device starts

- `WAKE_LOCK` :
        Used by the notification service

##### Authorization with the API

- `Read` : Read data from the account

- `Write` : Write messages

- `Follow` : Follow, unfollow, block, mute accounts

These actions are performed only by the user.

##### Tracking and Advertisements

The application does **not** use any tracking tools and does **not** run advertising.

##### Crash reports

The application uses [Acra](https://github.com/ACRA/acra) for crash reporting. This is disabled by default. When enabled from settings, after a crash, user will be asked to send an email with crash logs to the developer

##### Toot translations

By default the application uses [translate.fedilab.app](https://translate.fedilab.app/), a LibreTranslate instance hosted by ourselves. Fedilab also supports various third party translation services such as [Lingva](https://lingva.ml) and [DeepL](https://www.deepl.com). Privacy Policy of DeepL is available at [www.deepl.com/privacy.html](https://www.deepl.com/privacy.html)
