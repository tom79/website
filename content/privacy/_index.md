This website is hosted on [Codeberg Pages](https://codeberg.page). Their privacy policy is available at [codeberg.org/codeberg/org/src/PrivacyPolicy.md](https://codeberg.org/codeberg/org/src/PrivacyPolicy.md)

We don't collect any personal information through this website.
