+++
project = "Fedilab"
Title = "Installation"
+++
There are multiple ways to install Fedilab

1. **F-Droid**<br>
[f-droid.org/packages/fr.gouv.etalab.mastodon/](https://f-droid.org/packages/fr.gouv.etalab.mastodon/)

2. **Google Play**<br>
[play.google.com/store/apps/details?id=app.fedilab.android](https://play.google.com/store/apps/details?id=app.fedilab.android)

<!-- 3. **Build from source**<br>
If you're adventurous and use a GNU+Linux operating system, you can try to build the app from its source code. We have a tutorial to help you along the way.<br>
[Show build tutorial](../build) -->
