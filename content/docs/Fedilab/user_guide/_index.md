+++
project = "Fedilab"
Title = "User Guide"
+++
Hi! This is a walkthrough of Fedilab. We hope this would help you to get started with the app.

When you open Fedilab for the first time, you should see a page like this.

![The page where you enter the address of your instance](login/instance.jpg)

We'll show you how to proceed from here. Start with 'login', if you want to begin :)

1. [Login](login/)

2. [After login](after_login/)

3. [Basic UI](basic_ui/)

4. [Navigation Drawer](navigation_drawer/)

5. [Compose](compose/)

    - [Content Warning](compose/content_warning/)
    
    - [Adding Attachments](compose/add_attachments/)

    - [Managing Attachments](compose/manage_attachments/)

    - [Adding a Poll](compose/add_poll/)

    - [Custom Emojis](compose/custom_emoji/)

    - [Scheduling](compose/scheduling/)

    - [Threads](compose/threads/)

    - [More Compose Options](compose/more_compose_options/)
