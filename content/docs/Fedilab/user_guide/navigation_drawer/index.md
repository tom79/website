+++
project = "Fedilab"
Title = "Navigation Drawer"
+++
This is the 'navigation drawer' of Fedilab.

![Navigation drawer](./navigation_drawer.jpg)

To open it, you can either,
- Press on your avatar in the action bar in home page.
- Or swipe in from left side of the screen.

Let's see what's in the navigation drawer, from top to bottom.

- **Avatar**
![Avatar in navigation drawer](./avatar_in_navigation_drawer.jpg)
The avatar of your active account should appear here. Pressing on it will open your profile.

- **Display name and username area**<br>
![Display name and username area in navigation drawer](./display_name_username_area.jpg)
Pressing on this area will toggle the accounts list. It is explained in detail in '[Multiple Accounts](../multiple_accounts)' section.

- **instances.social button**<br>
![instances.social button in navigation drawer](./instance_social_button.jpg)
Pressing this button will open a dialog box, which should display information about your Fediverse instance using [instances.social](https://instances.social).

Below this, there are multiple categories in the navigation drawer. You can scroll down if some of them are hidden. Let's go through the items in each category.

#### My account
![My account category in navigation drawer](./category_my_account.jpg)
In this category, you can find items to access some commonly visited places when using Fediverse.

- **Drafts**<br>
If you have save any posts or replies as drafts, go here to manage those drafts.

- **Interactions**<br>
This will open an intermediate page that allows you to access these places.
![Interactions page](./interactions_page.jpg)
    - Bookmarks
    - Muted users
    - Home muted users<br>
    <small>(Explained in [Lists](../lists) section)</small>    
    - Blocked users
    - Favourites
    - Blocked domain

- **Filters**<br>
In here, you can manage filters of your Fediverse account.

- **Lists**<br>
A page to manage your 'lists'. See '[Lists](../lists)' section for more details about this page.

- **Followed tags**<br>
If you have followed any hash tags with your Mastodon account, those can be managed here

#### My instance
![My instance category in navigation drawer](./category_my_instance.jpg)
This category contains some items related to your Fediverse instance.

- **Announcements**<br>
Announcements from your instance's admin will appear in this page

- **Trending**<br>
Here you can find trending hashtags provided by your instance

- **Suggestions**<br>
Accounts recommended for you by your instance will be displayed in this page

- **About the instance**<br>
![Instance information dialog. We have used mstdn.social for this example](./about_instance.jpg)
This opens a dialog box that shows information about your instance, which you can find also on its 'about' page. The 'mail' button in this dialog allows you to send an email to your instance's admin using your email app.

#### My app
![My app category in navigation drawer](./category_my_app.jpg)
This category is dedicated to your app, Fedilab, related things.

- **Manage timelines**<br>
This page allows you to manage tabs in Fedilab's home page. Both action bar and navigation bar can be customized the way you like. To learn more you can read '[Manage Timelines](../manage_timelines)' section.

- **Scheduled**<br>
Go here to manage posts you have scheduled. More details can be found on '[Scheduling](../scheduling)' section.

- **Settings**<br>
Settings is.. settings. Fedilab provides many options for you to change the app the way you want. We will get there later, but if you want to read it now, go to '[Settings](../settings)' section.

- **Cache**<br>
By default, Fedilab caches timelines and media so you can view them offline. In here you have options to clear those cached content.<br>
_(If you don't need caching, you can disable it in settings)_

- **About**<br>
This opens the 'About' page of Fedilab, which has some links and information. Includes
    - Link to our website and source codes
    - Donation links
    - License information
    - Credits to some services used in the app

- **Release notes**<br>
Here you can view Fedilab's changelogs, in case you're curious to know what changes each release contains.

- **Partnerships**<br>
This page is dedicated for entities that help Fedilab. Just to be clear, there's no money or tracking involved here. If you have any questions about this, please feel free to ask us :)

In the next section, we explain how to compose a new post. If you like to continue with this guide, proceed to read the '[Compose](../compose/)' section.