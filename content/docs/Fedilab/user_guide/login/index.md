+++
project = "Fedilab"
Title = "Login"
+++
1. Type the address of your Fediverse instance in the text box
![The page where you enter the address of your instance](instance.jpg)

2. While you're typing the address, Fedilab will show you a list of known instance. If you see your instance in the list, press on it and Fedilab will fill the text box with that address.<br>_For this guide, we are going to use an account on mstdn.social._

![List of addresses of Fediverse servers are displayed based on what you are typing](suggestions.jpg)

3. Once you're done, press 'Continue'.

- Then Fedilab will open login page of your instance in your web browser.

4. Enter your credentials and sign into your account.

![Login page of the Mastodon instance we are using for this guide](login.jpg)

- Then your instance should ask if you want to authorize Fedilab to access your account. If you want to use Fedilab, press 'AUTHORIZE'.<br>_This step can be different, depending on your instance._

![Authorization page of a Mastodon instance](authorize.jpg)

- If you authorize the app, you will be taken back to Fedilab. Give Fedilab a little bit of time to talk with your instance. Once it's done, you've successfully logged in to your Fediverse account using Fedilab :)

If you like to continue with this guide, proceed to read the '[After Login](../after_login)' section.