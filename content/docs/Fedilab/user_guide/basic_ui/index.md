+++
project = "Fedilab"
Title = "Basic UI"
+++
This is the default home page of Fedilab.
![Default home screen](./home.jpg)

Let's get to know the basic parts of the app.

- At the top we have the 'action bar'.
![Action bar](./action_bar.jpg)

- On one side of the action bar you should see your account's avatar.
![Account avatar](./avatar.jpg)

- And the search icon on the other side. Pressing this will open the search page. 
![Search icon](./search_icon.jpg)

- If you have any 'lists' in your account, you should see the list tabs in between the search icon and your avatar. We will show you how to customize these tabs in '[Manage Timelines](../manage_timelines)' section.
![List tabs](./list_tabs.jpg)

- At the bottom we have the 'Navigation bar'.
![Navigation bar](./navigation_bar.jpg)

- By default, the items in the navigation bar are as below. These too are customizable.
![Default items of navigation bar](./navigation_bar_default_items.jpg)
    1. Home timeline
    2. Local timeline
    3. Federated timeline
    4. Notifications
    5. Conversations

- Just above the navigation bar, there is the compose button. To write a new post, press this button to open the compose page. To read more on how to make a new post, go to '[Compose](../compose)' section.
![Compose button](./compose_button.jpg)

- Last part to mention in this section is the 'Navigation drawer'. To open it, press your avatar on the action bar. Another way to open it is to swipe in from the left side of the screen. Navigation drawer provides access to lot of functions of Fedilab. We'll show you more about this on the next section.
![Navigation drawer](../navigation_drawer/navigation_drawer.jpg)

Now you got to know the basic UI of Fedilab. If you like to continue with this guide, proceed to read the '[Navigation Drawer](../navigation_drawer)' section.