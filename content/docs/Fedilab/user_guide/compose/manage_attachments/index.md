+++
project = "Fedilab"
Title = "Managing Attachments"
+++

![Attachments list](../add_attachments/attachment_area.jpg)
Attachments are displayed below the content area.

### Sensitive content
![Sensitive content option](./sensitive_content_option.jpg)
If your attachments contain sensitive content, consider enabling the "Sensitive content?" option. It will hide all attachments behind an additional click, so people who see your post will not see them by suprise.

---

Let's go through buttons in an attachment item.
![An attachment item](./attachment_item.jpg)

1. **Edit**<br>
This will open the image editor, where you can draw on the image, crop, add text, add filters, add emojis and more. Also in the image editor, you can set the focal point for the attachment's preview.

2. **Remove**<br>
Remove the attachment.

3. **Description**<br>
To add a content description for the attachment. We encourage you to add descriptions for every attachment. It will allow visually impaired people to understand the content. When you press the button, you'll get a dialog like below, where you can type and save the description.
![Content description dialog](./content_description_dialog.jpg)

### Reorder attachments
![Arrow buttons in attachments](./multiple_attachments.jpg)
If you have added more than one attachment, you'll get new arrow buttons in each attachment. You can use them to arrange the attachments in the order you want. When posting, Fedilab will upload them one after another, so they will be displayed in the correct order.

If you like to continue with this guide, proceed to read the '[Adding a Poll](../add_poll)' section.