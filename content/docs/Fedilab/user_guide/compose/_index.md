+++
project = "Fedilab"
Title = "Compose"
+++
If you're posting a new post or replying to a post, you will have to use the compose page.

![Compose page](compose.jpg)

To open this page, press on the compose button in home page.
![Compose button](../basic_ui/compose_button.jpg)


## Step 1: A simple post

First, we'll show you how to publish a simple post.

- **Add text**
![Content area](content_area.jpg)
Type the content of your post in this area.

- **Set visibility**
![Visbility button](visibility_button.jpg)
This button's icon indicates the visibility your post. If you want to change it, press it and you'll get a panel as below.
![Visibility panel](visibility_panel.jpg)
    - Press on the visibility you want for your post.
    - If you don't want to change it, press the 'x' icon at the bottom.

- **Publish**
![Publish button](publish_button.jpg)
When you're ready, press this button to publish it.

Done!

Now you know how to publish a simple post. There's more. In next sections, we explain other features such as content warnings, media attachments, polls, custom emojis and more. If you like to continue with this guide, proceed to read the '[Content Warning](content_warning/)' section.