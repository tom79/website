+++
project = "Fedilab"
Title = "Custom Emojis"
+++
Some Fediverse instances have custom emojis. If your instance provide them, you can use them when composing a post. There are two ways you can insert them.

- By typing a shortcode
- Using the custom emoji picker

1. **Typing a shortcode**<br>
    - Each custom emoji has its own 'shortcode'. You can start by typing a '`:`' (colon). Then type the shortcode. While you're typing Fedilab will show you suggestions, a list of custom emojis that matches the shortcode you're typing. See the next image.
    ![Sugggestions while typing a custom emoji shortcode](typing_shortcode.jpg)
    
    - If you see the custom emoji you wanted in that list, press on it and Fedilab will complete the shortcode you were typing.
    ![Emoji shortcode is autocompleted](shortcode_entered.jpg)

2. **Custom emoji picker**<br>
    - To open the custom emoji picker, press the custom emoji button in the compose page.
    ![Custom emoji button](custom_emoji_button.jpg)
    
    - Then you'll get a dialog like below, which will show you all the custom emojis provided by your instance.
    ![Custom emoji picker](custom_emoji_picker.jpg)

    - Press on the custom emoji you want to add and Fedilab will add its shortcode to the text area.
    ![Emoji shortcode is added to the text area](shortcode_entered.jpg)

If you like to continue with this guide, proceed to read the '[Scheduling](../scheduling/)' section.