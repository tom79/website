+++
project = "Fedilab"
Title = "Content Warning"
+++
### Content warning
- To add a content warning, press the button with an 'eye' icon.
![Content warning button](cw_button.jpg)

- When you press it, a new text box should appear above the content area.
![Content warning text box](./cw_text_area.jpg)

- You can write the warning message in that text box.

- To disable the CW, press the 'eye' icon again.

If you like to continue with this guide, proceed to read the '[Adding Attachments](../add_attachments/)' section.