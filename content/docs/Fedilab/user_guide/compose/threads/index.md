+++
project = "Fedilab"
Title = "Threads"
+++
1. To create a thread, prepare its first post and press the button with a '+' icon to add a new post.
![Button to add a new post](./thread_add_post_button.jpg)

2. Then you'll get a new post below the first post.
![A new post for the thread](./thread_second_post.jpg)

3. If you want to add more posts, repeat step 1, but on the last post.

4. If you want to remove the last post, press the button with a '-' icon.<br>
_Note that this button appears only when the post is empty_
![Button to remove the last post](./thread_second_post_remove.jpg)

If you like to continue with this guide, proceed to read '[More Compose Option](../more_compose_options/)' section.
