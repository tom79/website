+++
project = "Fedilab"
Title = "More Compose Options"
+++

### Post Language
![Language button](language_button.jpg)

This allows you to set the language of your post. Pressing this button will open a list of languages.

![Languages list](languages_list.jpg)

Select the language for your post and then press 'Validate' to save it.

### Camera
![Camera button](camera_button.jpg)
This button will open your phone camera app. When you take a photo, Fedilab will add it to your post as an attachment.

### Contacts
![Contacts button](contacts_button.jpg)

Using this option, you can search accounts your follow and add mention them.
Pressing this button to open the contacts dialog.

![Contacts dialog](contacts_dialog.jpg)

Then search the account you want to mention. You can check/uncheck each account to add/remove mentions.

### Emoji button
![Emoji button](emoji_button.jpg)
Pressing this button will open the emoji panel which looks like next image.<br>
_Note that these are standard emojis, **not** '[Custom Emojis](../custom_emoji/)'_

![Emoji panel](emoji_panel.jpg)

This is the end of compose section. If you like to continue with this guide, proceed to read '[Manage Timelines](../../manage_timelines/)' section.