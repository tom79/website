+++
project = "Fedilab"
Title = "Adding a Poll"
+++
- When composing a post, if you want to add attachments _(like photos, videos and audio)_ to your post, first press the button with a 'paper clip' icon.
![Attachment icon](../add_attachments/attachment_icon.jpg)

- It will open a small panel with options to add different types of attachments.
![Attachment choices](../add_attachments/attachment_choices.jpg)

- In this panel, press on the poll button.
![Poll button](poll_button.jpg)

- Then you'll get a dialog like this.
![Poll dialog](poll_dialog.jpg)


### Poll choices

- You can type text for poll's choices in text boxes that say 'Choice 1' and 'Choice 2'.
![Poll choices](poll_choices.jpg)

- If you want to add more choices, press the button that says 'Add a choice'.
![Button to add a new choice](poll_add_choice.jpg)

- Then you should get new choices as below image.
![Added choice in poll](poll_added_option.jpg)

- To remove a choice you added, press the 'x' icon next to its text box.
![Button to remove an added choice](poll_remove_added_option.jpg)

- Below the choices, you can set the type and the duration of your poll.
![Type of the poll](poll_type.jpg)
![Duration of the poll](poll_duration.jpg)

- When you're done creating the poll, press the 'Save' button to save the poll.
![Save button](poll_save.jpg)

- If you want to discard or remove the poll, press the 'Delete' button instead.
![Delete button](poll_delete.jpg)

- After adding a poll, you can edit it by pressing again the poll button in attachment choices.
![Poll button](poll_button.jpg)

If you like to continue with this guide, proceed to read the '[Custom Emoji](../custom_emoji)' section.