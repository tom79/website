+++
project = "Fedilab"
Title = "Scheduling"
+++
In this section we'll show you how to schedule a post.

Prepare your post as you normally would. Then, instead of posting, press the three-dot-menu button on top.
![three-dot-menu button](schedule_three_dot_menu.jpg)

Select 'Schedule' item from the menu.
![Schedule menu item](schedule_menu_item.jpg)

Now you'll get the schedule dialog with a date picker. Pick the date for your post and press the next button.
![Date picker](schedule_set_date.jpg)

Now you should see a time picker. Set the time for your post to be published. When you're done, press the button with a check mark to schedule the post.
![Time picker](schedule_set_time.jpg)

Now your post is sheduled. If you want to cancel it, open the navigation drawer and go to 'Scheduled' page. It's explained in '[Managing Scheduled Posts](../../manage_scheduled_posts/)' section.

If you like to continue with this guide, proceed to read the '[Threads](../threads/)' section.
