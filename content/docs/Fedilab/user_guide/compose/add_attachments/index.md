+++
project = "Fedilab"
Title = "Adding Attachments"
+++
When composing a post, if you want to add attachments _(like photos, videos and audio)_ to your post, first press the button with a 'paper clip' icon.

![Attachment icon](attachment_icon.jpg)

It will open a small panel with options to add different types of attachments.

![Attachment choices](attachment_choices.jpg)

The items in this panel are as below
![](attachment_choices_items.jpg)
1. **Image**<br>
Select a image/photo to attach

2. **Audio**<br>
Select an audio file to attach

3. **Video**<br>
Select a video to attach

4. **Close**<br>
Close the attachment choices panel

5. **Poll**<br>
To add a poll to your post.<br>_We will explain this later, but if you want to read it now, go to '[Adding a poll](../add_poll)' section._

6. **Other**<br>
If you want to attach a file of some other type, use this one.<br>
_Note that this option may not work in some Fediverse instances_


Let's see what happens when you add an attachment. For this guide, we decided to attach an image. When you add attachments, they will be displayed below the content area.

![Attachments list](./attachment_area.jpg)

If you like to continue with this guide, proceed to read the '[Manage Attachments](../manage_attachments)' section.