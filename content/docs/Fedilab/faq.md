+++
project = "Fedilab"
Title = "FAQ"
+++
{{<faq "How can I start using Fedilab's F-Droid repo?">}}
1. Copy below link

<https://fdroid.fedilab.app/repo?fingerprint=11F0A69910A4280E2CD3CCC3146337D006BE539B18E1A9FEACE15FF757A94FEB>

2. Open F-Droid app.
3. Go to its 'settings' page.
4. Go to 'Repositories'.
5. Press the '+' icon on top.
6. Now F-Droid app will show a dialog with the title 'Add new repository'. It will have its input fields auto filled with values from the copied link.
7. Press 'OK'.

- Now you have successfully added Fedilab's repo into F-Droid.

_Note that if you have already installed Fedilab from F-Droid, you'll have to uninstall it first and then install a version from our repo_
{{</faq>}}

{{<faq "How to switch between F-Droid, Play Store, Custom F-Droid Repo without losing data?">}}
1. First export your data from settings. It will create a file you will need later
2. Uninstall existing Fedilab app from your phone
3. Then install Fedilab from the source you want
4. Open Fedilab
5. In the login page, tap on three-dot icon, then tap on "Import"
6. Select the file you exported in first step

- Now you should have everything in Fedilab the same as you had before switching.
{{</faq>}}

{{<faq "What is 'Push Notifications' and how can I use it?">}}
"[Push Notifications](https://en.wikipedia.org/wiki/Push_technology#Web_push)" is an efficient way to get real-time notifications.

How to use it? (**Only for F-Droid users**)<br>
_(If you installed Fedilab from Google Play, you probably already have Push Notifications and can ignore this section)_

1. Install a 'distributor' app:
    1. We recommend [Sunup](https://f-droid.org/en/packages/org.unifiedpush.distributor.sunup/) as it is very easy to use
    2. [ntfy](https://f-droid.org/packages/io.heckel.ntfy/) is another easy solution, but you have to use another server if you get rate-limited
    3. You can find other distributors and more information on [unifiedpush.org](https://unifiedpush.org/)
2. Open the distributor app once after installing
3. Restart Fedilab

- Now Fedilab should be able to receive Push Notifications
{{</faq>}}
